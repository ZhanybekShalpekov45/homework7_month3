from aiogram import types
from config import bot, scheduler
from datetime import datetime


async def handle_notify(chat_id: int, reminder_text: str):
    await send_notification(chat_id, reminder_text)


async def notify(message: types.Message):
    user_id = str(message.from_user.id)
    if scheduler.get_job(user_id) is not None:
        await message.answer('Вы уже установили напоминание')
    else:
        reminder_text = message.text.replace('/notify', '').replace('Напомни', '').strip()
        if not reminder_text:
            await message.answer("Пожалуйста, укажите текст для напоминания.")
            return

        scheduler.add_job(
            handle_notify,
            "interval",
            seconds=1,
            args=(message.chat.id, reminder_text)  # Передаем chat_id и reminder_text как аргументы функции
        )
        await message.answer('Запрос, принят')


async def send_notification(chat_id: int, reminder_text: str):
    await bot.send_message(
        chat_id=chat_id,
        text=f"Напоминание: {reminder_text}"
    )
