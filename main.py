import logging

from aiogram import executor

from config import dp, scheduler
from handlers.notifications import notify
from handlers.group_admin import example


# PEP 8
if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    # планировщик
    dp.register_message_handler(notify)

    # в самом конце
    dp.register_message_handler(example)

    scheduler.start()
    executor.start_polling(
        dp,
        skip_updates=True
    )
